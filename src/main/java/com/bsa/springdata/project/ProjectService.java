package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        Pageable pageable = PageRequest.of(0, 5);
        return projectRepository.findTop5ByTechnology(technology, pageable).stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
    }

    public Optional<ProjectDto> findTheBiggest() {
        Pageable pageable = PageRequest.of(0, 1);
        return projectRepository.findTheBiggest(pageable)
                .stream()
                .map(ProjectDto::fromEntity)
                .findFirst();
    }



    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
    }

    @Transactional
    public UUID createWithTeamAndTechnology(CreateProjectRequestDto dto) {
        var tech = Technology.builder()
                .description(dto.getTechDescription())
                .link(dto.getTechLink())
                .name(dto.getTech())
                .build();
        var team = Team.builder()
                .area(dto.getTeamArea())
                .name(dto.getTeamName())
                .room(dto.getTeamRoom())
                .technology(tech)
                .build();
        var project = Project.builder()
                .name(dto.getProjectName())
                .description(dto.getProjectDescription())
                .build();
        project.addTeam(team);

        technologyRepository.save(tech);
        teamRepository.save(team);
        return projectRepository.save(project).getId();
    }
}
