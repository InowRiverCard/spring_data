package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

//    Native query version
//    @Query(value = "SELECT  count(*) FROM " +
//                   "(SELECT FROM projects p LEFT JOIN teams team on p.id = team.project_id " +
//                   "    LEFT JOIN users u on team.id = u.team_id " +
//                   "    JOIN user2role ON user2role.user_id = u.id " +
//                   "    JOIN roles r on r.id = user2role.role_id " +
//                   "WHERE r.name = :role " +
//                   "GROUP by p.id) as rows",
//            nativeQuery = true)
//    int getCountWithRole(String role);

    @Query(value = "SELECT count( distinct p) FROM Project p " +
                   "    INNER JOIN p.teams team " +
                   "    INNER JOIN team.users u " +
                   "    JOIN u.roles r " +
                   "WHERE r.name = :role ")
    int getCountWithRole(String role);

    @Query("SELECT p FROM Project p INNER JOIN p.teams t INNER JOIN t.users u " +
            "GROUP BY p.id, t.technology.name having t.technology.name = :technology order by count (u) desc ")
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);

    @Query("SELECT p FROM Project p INNER JOIN p.teams t INNER JOIN t.users u " +
            "GROUP BY p.id ORDER BY COUNT (u) DESC , p.name DESC ")
    List<Project> findTheBiggest(Pageable pageable);

    @Query(value = "SELECT p.name, count(DISTINCT team) AS teamsNumber," +
                   "    count(u) as developersNumber, string_agg(distinct tech.\"name\", ',') AS technologies " +
                   "FROM projects p " +
                   "    INNER JOIN teams team ON p.id = team.project_id " +
                   "    INNER JOIN users u ON u.team_id = team.id " +
                   "    INNER JOIN technologies tech ON tech.id = team.technology_id " +
                   "GROUP BY p.id",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}