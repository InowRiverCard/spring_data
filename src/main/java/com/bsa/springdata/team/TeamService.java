package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var newTech = technologyRepository.findByName(newTechnologyName).orElseThrow();
        var teams = teamRepository.findByUserCountAndTechnology(devsNumber, oldTechnologyName);
        var updatedTeams = teams.stream()
                .peek(team -> team.setTechnology(newTech))
                .collect(Collectors.toList());
        teamRepository.saveAll(updatedTeams);
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
