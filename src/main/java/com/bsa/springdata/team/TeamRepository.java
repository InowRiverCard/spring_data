package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    int countByTechnologyName(String technology);


    @Query("SELECT t from Team t inner join t.users u inner join t.technology" +
            " GROUP BY t.id, t.technology.name having count(u) < :userCount and t.technology.name = :technologyName")
    List<Team> findByUserCountAndTechnology(long userCount, String technologyName);

    @Transactional
    @Modifying
    @Query(value = "update teams as team " +
                "set " +
                "   name = Concat(t.t_name, '_', t.p_name, '_', t.tech_name) " +
                "from (select team.name as t_name, p.name as p_name, tech.name as tech_name " +
                "   from teams as team " +
                "   inner join projects as p on p.id = team.project_id " +
                "   inner join technologies as tech on tech.id = team.technology_id " +
                "   where team.name = :name) as t " +
                "where team.name = :name ",
            nativeQuery = true
    )
    void normalizeName(String name);

    Optional<Team> findByName(String name);
}
