package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select distinct o from Office o left join o.users u where u.team.technology.name = :technology")
    List<Office> getByTechnology(String technology);

    @Transactional
    @Modifying
    @Query("update Office o set o.address = :newAddress where o.address = :oldAddress and " +
            "0 < (select count(u) From User u inner join u.office o where o.address = :oldAddress)")
    int updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);
}
